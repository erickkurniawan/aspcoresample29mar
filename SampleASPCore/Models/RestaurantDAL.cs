﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace SampleASPCore.Models
{
    public class RestaurantDAL : IRestaurant
    {
        private SqlConnection conn;
        private IConfiguration _config;

        public RestaurantDAL(IConfiguration config)
        {
            _config = config;
        }

        private string GetConnStr()
        {
            return _config.GetConnectionString("DefaultConnection");
        }

        public bool Delete(string id)
        {
            using (conn = new SqlConnection(GetConnStr()))
            {
                string strSql = @"delete from Restaurants where ID=@ID";
                var param = new { ID = id };
                try
                {
                    int result = conn.Execute(strSql, param);
                    if (result == 1)
                        return true;
                    else
                        return false;
                }
                catch (SqlException sqlEx)
                {
                    throw new Exception(sqlEx.Message);
                }
            }
        }

        public Restaurant Edit(string id, Restaurant obj)
        {
            using (conn = new SqlConnection(GetConnStr()))
            {
                string strSql = @"update Restaurants set Name=@Name where ID=@ID";
                var param = new { Name = obj.Name, ID = id };
                try
                {
                    conn.Execute(strSql, param);
                    obj.ID = Convert.ToInt32(id);
                    return obj;
                }
                catch (SqlException sqlEx)
                {
                    throw new Exception(sqlEx.Message);
                }
            }
        }

        public IEnumerable<Restaurant> GetAll()
        {
            IEnumerable<Restaurant> results;
            using(conn = new SqlConnection(GetConnStr()))
            {
                string strSql = @"select * from Restaurants order by Name asc";
                results = conn.Query<Restaurant>(strSql);
            }
            return results;
        }

        public Restaurant GetByID(string id)
        {
            Restaurant result;
            using (conn = new SqlConnection(GetConnStr()))
            {
                string strSql = @"select * from Restaurants where ID=@ID";
                var param = new { ID = Convert.ToInt32(id) };
                result = conn.QuerySingle<Restaurant>(strSql, param);
                return result;
            }
        }

        public IEnumerable<Restaurant> GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public Restaurant Insert(Restaurant obj)
        {
            using (conn = new SqlConnection(GetConnStr()))
            {
                string strSql = @"insert into Restaurants(Name) values(@Name);select @@identity";
                var param = new { Name = obj.Name };

                try
                {
                    int id = conn.ExecuteScalar<int>(strSql,param);
                    obj.ID = id;
                    return obj;
                }
                catch (SqlException sqlEx)
                {
                    throw new Exception(sqlEx.Message);
                }
            }
        }

        public IEnumerable<RestaurantWithCategoryVM> GetAllWithCategory()
        {
            using (conn = new SqlConnection(GetConnStr()))
            {
                string strSql = @"select r.ID,r.Name,c.CategoryID,c.CategoryName 
                                  from Restaurants r inner join Categories c on r.CategoryID=c.CategoryID 
                                  order by r.Name asc";
                return conn.Query<RestaurantWithCategoryVM>(strSql);
            }
        }

        //menggunakan dapper
        public IEnumerable<Restaurant> GetAllWithCategoryDapper()
        {
            using (conn = new SqlConnection(GetConnStr()))
            {
                //string strSql = @"SELECT * 
                //                FROM dbo.Restaurants LEFT OUTER JOIN dbo.Categories ON 
                //                dbo.Categories.CategoryID = dbo.Restaurants.CategoryID";

                var result = conn.Query<Restaurant, Category, Restaurant>("SP_SelectRestaurantWithCategory", 
                    (r, c) =>
                {
                    r.Category = c;
                    return r;
                }, splitOn: "CategoryID",commandType:System.Data.CommandType.StoredProcedure);

                return result;
            }
        }
    }
}
