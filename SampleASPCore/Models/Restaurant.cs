﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCore.Models
{
    public class Restaurant
    {
        public int ID { get; set; }
        public int CategoryID { get; set; }
        public string Name { get; set; }

        public Category Category { get; set; }
    }
}
