﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCore.Models
{
    public interface ICrud<T>
    {
        IEnumerable<T> GetAll();
        T GetByID(string id);
        T Insert(T obj);
        T Edit(string id, T obj);
        bool Delete(string id);
    }
}
