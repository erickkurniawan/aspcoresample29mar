﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleASPCore.Models
{
    public interface IRestaurant : ICrud<Restaurant>
    {
        IEnumerable<Restaurant> GetByName(string name);
        IEnumerable<RestaurantWithCategoryVM> GetAllWithCategory();
        IEnumerable<Restaurant> GetAllWithCategoryDapper();
    }
}
