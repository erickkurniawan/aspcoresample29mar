﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SampleASPCore.Models;

namespace SampleASPCore.Controllers
{
    public class RestaurantController : Controller
    {
        private IRestaurant _restaurant;
        public RestaurantController(IRestaurant restaurant)
        {
            _restaurant = restaurant;
        }

        // GET: Restaurant
        public ActionResult Index()
        {
            ViewBag.Pesan = TempData["Pesan"];
            var model = _restaurant.GetAll();
            return View(model);
        }

        // GET: Restaurant/Details/5
        public ActionResult Details(string id)
        {
            var model = _restaurant.GetByID(id);
            return View(model);
        }

        // GET: Restaurant/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Restaurant/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(new string[] { "Name" })]Restaurant restaurant)
        {
            try
            {
                _restaurant.Insert(restaurant);
                TempData["Pesan"] = $"<span class='alert alert-success'>Data {restaurant.Name} berhasil ditambahkan !</span>"; 
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                ViewBag.Pesan = "<span class='alert alert-danger'>Gagal menambahkan Data Restaurant</span>";
                return View();
            }
        }

        // GET: Restaurant/Edit/5
        public ActionResult Edit(string id)
        {
            var model = _restaurant.GetByID(id);
            return View(model);
        }

        // POST: Restaurant/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string id, Restaurant restaurant)
        {
            try
            {
                _restaurant.Edit(id, restaurant);
                TempData["Pesan"] = $"<span class='alert alert-success'>Data {restaurant.Name} berhasil diedit !</span>";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                ViewBag.Pesan = "<span class='alert alert-danger'>Gagal mengedit Data Restaurant</span>";
                return View(restaurant);
            }
        }

        // GET: Restaurant/Delete/5
        public ActionResult Delete(string id)
        {
            var model = _restaurant.GetByID(id);
            return View(model);
        }

        // POST: Restaurant/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePost(string id)
        {
            try
            {
                // TODO: Add delete logic here
                _restaurant.Delete(id);
                TempData["Pesan"] = $"<span class='alert alert-success'>Data berhasil didelete !</span>";
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public IActionResult RestaurantWithCategory()
        {
            var model = _restaurant.GetAllWithCategory();
            return View(model);
        }

        public IActionResult RestaurantWithCategoryDapper()
        {
            var model = _restaurant.GetAllWithCategoryDapper();
            return View(model);
        }
    }
}